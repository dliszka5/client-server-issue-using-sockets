#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <sys/errno.h>

#include <arpa/inet.h>

#include <strings.h> 	// for bzero

#include <signal.h>

#include <string.h>

#include <pthread.h>

/*
       To accept connections, the following steps are performed:

           1.  A socket is created with socket(2).

           2.  The socket is bound to a local address using bind(2), so that
               other sockets may be connect(2)ed to it.

           3.  A willingness to accept incoming connections and a queue
               limit for incoming connections are specified with listen().

           4.  Connections are accepted with accept(2).
*/

#define PORT 8080
#define MAX_CLIENT 10

#define MAX_LENGTH 1000

#define true 1

void convertToUpperCase(char*);
void *connection_handler(void *);
void interrupt_handler(int);

int socketfd;

int main(){

/*	
	AF_INET             IPv4 Internet protocols          ip(7)
    AF_INET6            IPv6 Internet protocols 
*/

	fflush(stdout);
	int client_socket;

	struct sockaddr_in server;
	struct sockaddr_in client;

	socketfd = socket(AF_INET,SOCK_STREAM,0);

    signal(SIGINT,interrupt_handler);

	if(socketfd == -1){
		perror("socket error");
		exit(EXIT_FAILURE);
	}

	bzero((char *) &server, sizeof(server));

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(PORT);	// convert number to port

    if(bind(socketfd, (struct sockaddr*)&server, sizeof(server)) == -1){
    	perror("bind error");
    	exit(EXIT_FAILURE);
    }
    puts("bind done");

    if(listen(socketfd,MAX_CLIENT) == -1){
    	perror("listen error");
    	exit(EXIT_FAILURE);
    }
    puts("listen done");

    int c = sizeof(struct sockaddr_in);

    while(client_socket = accept(socketfd,(struct sockaddr*)&client,(socklen_t *)&c)){
        if(client_socket == -1){
        	perror("accept error");
        	exit(EXIT_FAILURE);
        }
         
        pthread_t client_thread;
        int* new_sock = malloc(1);
        *new_sock = client_socket;
         
        if( pthread_create( &client_thread , NULL ,  connection_handler , (void*) new_sock) < 0){
            perror("pthread create error");
            exit(EXIT_FAILURE);
        }
         
        //Now join the thread , so that we dont terminate before the thread
        //pthread_join( sniffer_thread , NULL);
        puts("Handler assigned");
    }


    // convert to uppercase 
   	// then
   	// write to client socket
   	// write(client_sock , client_message , strlen(client_message)); 

    close(socketfd);
	return 0;
}

void convertToUpperCase(char sPtr[]){
	int i=0;
	while(sPtr[i] != '\0'){
		if(sPtr[i] >= 97)
			if(sPtr[i]<=122)
				sPtr[i] = sPtr[i] - 32;
		++i;
	}
}

void *connection_handler(void *socketfd){
    char client_message[MAX_LENGTH];
    int read_size = 0;

    int client_socket = *(int*)socketfd;

    //Receive a message from client
    while((read_size = recv(client_socket , client_message , MAX_LENGTH , 0)) > 0 ){

        printf("Recived : %s\n",client_message);
        fflush(stdout);

        convertToUpperCase(client_message);
        printf("Converted to : %s\n",client_message);
        fflush(stdout);

        write(client_socket , client_message , strlen(client_message)+1);
    }
     
    if(read_size == 0){
        perror("client disconnected");
        fflush(stdout);
    }
    else if(read_size == -1){
        perror("recv failed");
    }

    free(socketfd);
}

void interrupt_handler(int signal){ 
    if (signal == SIGINT) {
        if(close(socketfd) == -1){
            perror("close error");
            exit(EXIT_FAILURE);
        }
        printf("Server socket closed\n");
        exit(0);
    }
}