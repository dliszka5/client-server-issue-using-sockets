#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include <sys/socket.h>    	//socket
#include <arpa/inet.h> 		  //inet

#include <sys/errno.h>

#include <string.h>

#include <signal.h>

/*
       To accept connections, the following steps are performed:

           1.  A socket is created with socket(2).

           2.  The socket is bound to a local address using bind(2), so that
               other sockets may be connect(2)ed to it.

           3.  A willingness to accept incoming connections and a queue
               limit for incoming connections are specified with listen().

           4.  Connections are accepted with accept(2).
*/

#define PORT 8080
#define MAX_CLIENT 10

#define MAX_LENGTH 1000

#define true 1

int socketfd;
void interrupt_handler(int);

int main(){

	struct sockaddr_in server;

	char* message;
	char server_respond[MAX_LENGTH];

	socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if(socketfd == -1){
		perror("socket error");
		exit(EXIT_FAILURE);
	}

  signal(SIGINT,interrupt_handler);

	  server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);

    /*
		int connect(int socket, const struct sockaddr *address,
		socklen_t address_len);
    */

    if(connect(socketfd,(struct sockaddr*)&server,sizeof(server)) == -1){
    	perror("connect error");
    	exit(EXIT_FAILURE);
    }

    puts("Connected with remote server ! ");
    fflush(stdout);

    while(true){
    	printf("[C] Enter a message : ");
    	scanf("%s",message);
    	
    	if(send(socketfd,message,strlen(message)+1,0) == -1){
    		perror("\nsend error\n");
    		break;
    	}

      
      if(recv(socketfd , server_respond , MAX_LENGTH , 0) == -1){
          perror("\nrecv error\n");
          break;
      }

      printf("[C] Recived : %s\n",server_respond);
      
         
    }

    close(socketfd);

	return 0;
}

void interrupt_handler(int signal){ 
    if (signal == SIGINT) {
        if(close(socketfd) == -1){
            perror("close error");
            exit(EXIT_FAILURE);
        }
        printf("Server socket closed\n");
        exit(0);
    }
}